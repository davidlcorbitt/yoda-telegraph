rm -f yoda-translations_prepared.jsonl \
&& cp ~/downloads/"training-data - yoda.csv" ./yoda-translations.csv \
&& openai tools fine_tunes.prepare_data -f yoda-translations.csv \
&& mv ~/downloads/"training-data - yoda.csv" ~/downloads/"training-data - yoda_used.csv" \
&& openai api fine_tunes.create -t "yoda-translations_prepared.jsonl" -m babbage