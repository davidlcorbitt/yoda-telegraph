-- CreateTable
CREATE TABLE "Group" (
    "id" INTEGER NOT NULL,
    "lastMessage" TEXT NOT NULL DEFAULT E'Hello.',
    "muted" BOOLEAN NOT NULL DEFAULT true,
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,

    CONSTRAINT "Group_pkey" PRIMARY KEY ("id")
);
