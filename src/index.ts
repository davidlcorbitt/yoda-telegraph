import { Telegraf } from "telegraf"
const makeHandler = require("lambda-request-handler")
import { APIGatewayProxyEvent, APIGatewayProxyResult } from "aws-lambda"

require("dotenv").config()
import {
  getGroup,
  setGroupMuted,
  setLastMessageInGroup,
} from "./services/groups"
import { getTranslation } from "./services/translations"

const bot = new Telegraf(process.env.BOT_TOKEN)

async function setup() {
  bot.launch()
}
setup()

bot.start((ctx) => {
  ctx.reply("Welcome!")
})
bot.help((ctx) => ctx.reply("Send me a sticker"))
bot.on("sticker", (ctx) => ctx.reply("👍"))
bot.hears("hi", (ctx) => ctx.reply("Hey there"))
bot.hears(/.*[rR]ight [yY]oda.*/, async (ctx) => {
  const group = await getGroup({ id: ctx.update.message.chat.id })
  if (group && group.lastMessage) {
    const response = await getTranslation(group.lastMessage)
    ctx.reply(response)
  } else {
    ctx.reply("Think more, I must.")
  }
})

bot.hears(/[yY]oda [mM]ute/, async (ctx) => {
  await setGroupMuted({ id: ctx.update.message.chat.id, muted: true })
})
bot.hears(/[yY]oda [rR]epeat/, async (ctx) => {
  await setGroupMuted({ id: ctx.update.message.chat.id, muted: false })
})
bot.hears(/[pP]hilosophize/, async (ctx) => {
  // TODO: add a way to get a random quote
  // const index = Math.floor(Math.random() * yodaQuotes.length)
  // // const index = quotes.length - 1;
  // const response = await getTranslation(yodaQuotes[index])
  // ctx.reply(response)
  ctx.reply("TODO")
})
bot.hears(/[yY]oda [hH]elp/, async (ctx) => {
  const response = `
yoda help - display instructions for Yoda Corbitt

yoda repeat - set Yoda to repeat everything that is said

yoda mute - set Yoda to only speak when spoken to

right yoda? - cause Yoda to agree with the last message sent by anyone in the group
`
  ctx.reply(response)
})

bot.on("message", async (ctx) => {
  const group = await getGroup({ id: ctx.update.message.chat.id })
  if (group && !group.muted) {
    const response = await getTranslation(ctx.update.message.text)
    ctx.reply(response)
  }
  setLastMessageInGroup({
    id: ctx.update.message.chat.id,
    lastMessage: ctx.update.message.text,
  })
})

console.log("Yoda started")

// export const lambdaHandler = async (
//   event: APIGatewayProxyEvent
// ): Promise<APIGatewayProxyResult> => {
//   await bot.webhookCallback("/")
//   return {
//     statusCode: 200,
//     body: JSON.stringify({}),
//   }
// }

export const handler = makeHandler(bot.webhookCallback("/"))
