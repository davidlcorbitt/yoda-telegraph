import type { Prisma } from "@prisma/client"

import { db } from "../lib"

async function getGroup({ id }: Prisma.GroupWhereUniqueInput) {
  return db.group.findUnique({ where: { id } })
}

interface SetGroupMutedArgs {
  id: number
  muted: boolean
}
async function setGroupMuted({ id, muted }: SetGroupMutedArgs) {
  return db.group.upsert({
    where: { id },
    update: { muted },
    create: { id, muted },
  })
}

interface SetLastMessageInGroupArgs {
  id: number
  lastMessage: string
}
async function setLastMessageInGroup({
  id,
  lastMessage,
}: SetLastMessageInGroupArgs) {
  return db.group.upsert({
    where: { id },
    update: { lastMessage },
    create: { id, lastMessage },
  })
}

export { getGroup, setGroupMuted, setLastMessageInGroup }
