const axios = require("axios")
const { Configuration, OpenAIApi } = require("openai")

const configuration = new Configuration({
  apiKey: process.env.OPENAI_API_KEY,
})
const openai = new OpenAIApi(configuration)

async function getTranslation(text) {
  // FINE TUNED MODEL
  try {
    const response = await openai.createCompletionFromModel({
      model: process.env.FINE_TUNED_MODEL,
      prompt: generatePrompt(text),
      stop: "###",
      temperature: 0,
      max_tokens: text.split(" ").length * 2 + 10,
    })

    if (response?.data?.choices && response.data.choices[0]?.text) {
      return response.data.choices[0].text
    }
  } catch (e) {
    console.log(e)
  }

  return "Hrmmmm, much trouble this brings me."

  // BASE DA VINCI MODEL
  // const completion = await openai.createCompletion("text-davinci-002", {
  //   prompt: generateYodaPrompt(text),
  //   temperature: 0,
  //   stop: "###",
  // })
  // return completion.data.choices[0].text

  // THING I CREATED
  // return axios
  //   .post("http://localhost:8000/yoda", {
  //     text: text,
  //   })
  //   .then((res) => {
  //     // console.log(res)
  //     console.log(`statusCode: ${res.statusCode}`)
  //     console.log("Translation is:")
  //     console.log(res.data)
  //     return res.data
  //   })
  //   .catch((error) => {
  //     console.error(error)
  //     return "Hrmmmm, much trouble this brings me."
  //   })
}

function generateYodaPrompt(text) {
  return `
  Tell me how Yoda would say:

  ${text}\n\n###\\n\n
  `
}

function generatePrompt(text) {
  return text + ".\n\n###\n\n"
}

export { getTranslation }
